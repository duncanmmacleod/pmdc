# -*- coding: utf-8 -*-

from setuptools import setup

version = "1.0.2"

setup(name="python-pmdc",
      description="LIGO pmdc",
      version=version,
      author="Jeff Kline",
      author_email="ldr@ligo.org",
      license="GPLv3",
      py_modules=["pmdc"],
      setup_requires=["setuptools"],
      entry_points={
          "console_scripts": [
              "pmdc=pmdc:main",
          ],
      },
      data_files=[
          ("share/man/man8", ["man/pmdc.8"]),
      ],
      classifiers=[
          'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
      ],
)
