%define srcname pmdc
%define version 1.0.2
%define release 1

Name: python-%{srcname}
Version: %{version}
Release: %{release}%{?dist}
Summary: LIGO Poor Man's DiskCache
License: GPLv3+
Url: https://git.ligo.org/duncanmmacleod/python-pmdc
Vendor: Duncan Macleod <duncan.macleod@ligo.org>

BuildArch: noarch
Prefix: %{_prefix}

Source0: https://software.igwn.org/sources/source/%{name}-%{version}.tar.gz

# build
BuildRequires: python-rpm-macros
BuildRequires: python2
BuildRequires: python2-rpm-macros
BuildRequires: python2-setuptools

%description
Efficiently search file systems for frames, cache results in various formats.

# -- python2-pmdc

%package -n python2-%{srcname}
Summary: Python %{python2_version} PMDC library
Requires: python2
%{?python_provide:%python_provide python2-%{srcname}}
%description -n python2-%{srcname}
Efficiently search file systems for frames, cache results in various formats.
This package provides the Python %{python2_version} library.

%package -n %{srcname}
Summary: %{summary}
Requires: python2-%{srcname} = %{version}-%{release}
%description -n %{srcname}
Efficiently search file systems for frames, cache results in various formats.
This package provides the command-line interfaces

# -- build steps

%prep
%setup -q -n %{name}-%{version}

%build
%py2_build

%install
%py2_install "--install-scripts=/usr/sbin"

# -- files

%files -n python2-%{srcname}
%doc README.rst
%license LICENSE
%{python2_sitelib}/*

%files -n %{srcname}
%doc README.rst
%license LICENSE
%{_sbindir}/*
%{_mandir}/*/*

# -- changelog

%changelog
* Thu Mar 24 2022 Duncan Macleod <duncan.macleod@ligo.org> - 1.0.2-1
- first cut at RHEL packaging
