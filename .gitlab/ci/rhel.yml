# ---------------------------
# RHEL packaging workflow
# ---------------------------

include:
  # https://computing.docs.ligo.org/gitlab-ci-templates/
  - project: computing/gitlab-ci-templates
    # https://computing.docs.ligo.org/gitlab-ci-templates/rhel/
    file: rhel.yml
  # local test template
  - local: /.gitlab/ci/test.yml

# -- macros

.el7:
  image: sl:7
  variables:
    EPEL: "true"

.el8:
  image: rockylinux:8
  variables:
    EPEL: "true"

# -- source packages --------
#
# These jobs make src RPMs
#

.srpm:
  extends:
    # https://computing.docs.ligo.org/gitlab-ci-templates/rhel/#.rhel:srpm
    - .rhel:srpm
  stage: Source packages
  needs:
    - tarball
  variables:
    TARBALL: "python-pmdc-*.tar.*"

srpm:el7:
  extends:
    - .srpm
    - .el7

srpm:el8:
  extends:
    - .srpm
    - .el8

# -- binary packages --------
#
# These jobs generate binary RPMs
# from the src RPMs
#

.rpm:
  extends:
    # https://computing.docs.ligo.org/gitlab-ci-templates/rhel/#.rhel:rpm
    - .rhel:rpm
  stage: Binary packages
  variables:
    SRPM: "python-pmdc-*.src.rpm"

rpm:el7:
  extends:
    - .rpm
    - .el7
  needs:
    - srpm:el7

rpm:el8:
  extends:
    - .rpm
    - .el8
  needs:
    - srpm:el8

# -- test -------------------

.test:el:
  extends:
    # see /.gitlab/ci/test.yml
    - .test
  variables:
    PYTHON: "python2"
  before_script:
    # set up yum caching
    - !reference [".rhel:base", before_script]
    # install testing requirements
    - yum -y -q install which
    - if [[ "${CI_JOB_NAME}" == *"el7" ]]; then
          yum -y -q install python-coverage;
      else
          yum -y -q install python2-coverage;
      fi
    # install our package(s)
    - yum -y -q install *.rpm

test:el7:
  extends:
    - .test:el
    - .el7
  needs:
    - rpm:el7

test:el8:
  extends:
    - .test:el
    - .el8
  needs:
    - rpm:el8

# -- lint -------------------
#
# These jobs check the code
# for quality issues
#

.rpmlint:
  extends:
    # https://computing.docs.ligo.org/gitlab-ci-templates/rhel/#.rhel:lint
    - .rhel:lint
  stage: Code quality
  variables:
    GIT_STRATEGY: fetch
    RPMLINT_OPTIONS: '--info --file .rpmlintrc'

rpmlint:el7:
  extends:
    - .rpmlint
    - .el7
  needs:
    - rpm:el7

rpmlint:el8:
  extends:
    - .rpmlint
    - .el8
  needs:
    - rpm:el8
